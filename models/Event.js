const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EventSchema = new Schema({
  nome: String,
  endereco: String,
  numeroTelefone: String,
  horario: String,
  informacoes: String,
  imagens: [String],
  categorias: [String], 
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;