const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LocalSchema = new Schema({
  nome: String,
  endereco: String,
  numeroTelefone: String,
  horario: String,
  informacoes: String,
  imagens: [String],
  categorias: [String], 
});

const Local = mongoose.model('Local', LocalSchema);

module.exports = Local;