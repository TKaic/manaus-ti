require("./config/db");

const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

const UserRouter = require("./api/User");

const LocalRouter = require("./api/Local");

const EventRouter = require("./api/Event");


const bodyParser = require("express").json;
app.use(bodyParser());

const cors = require("cors");
app.use(cors());

app.use("/user", UserRouter);

app.use("/local", LocalRouter);

app.use("/event", EventRouter);

app.use(express.static(__dirname + '/public'));

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
