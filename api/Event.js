const express = require('express');
const router = express.Router();
const Event = require('./../models/Event');

router.post('/create', (req, res) => {
  const { endereco, numeroTelefone, horario, informacoes, imagens, nome, categorias } = req.body;

  const newEvent = new Event({
    nome,
    endereco,
    numeroTelefone,
    horario,
    informacoes,
    imagens,
    categorias
  });

  newEvent
    .save()
    .then((result) => {
      res.json({
        status: 'SUCCESS',
        message: 'Event created successfully',
        data: result,
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while creating a Event',
      });
    });
});

router.put('/edit/:id', (req, res) => {
  const { endereco, numeroTelefone, horario, informacoes } = req.body;

  Event.findByIdAndUpdate(
    req.params.id,
    {
      endereco,
      numeroTelefone,
      horario,
      informacoes,
    },
    { new: true }
  )
    .then((result) => {
      res.json({
        status: 'SUCCESS',
        message: 'Event updated successfully',
        data: result,
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while updating the Event',
      });
    });
});

router.delete('/delete/:id', (req, res) => {
  Event.findByIdAndDelete(req.params.id)
    .then(() => {
      res.json({
        status: 'SUCCESS',
        message: 'Event deleted successfully',
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while deleting the Event',
      });
    });
});

router.get('/get/:id', (req, res) => {
  Event.findById(req.params.id)
    .then((result) => {
      res.json({
        status: 'SUCCESS',
        data: result,
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while fetching the Event',
      });
    });
});

router.get('/getall', (req, res) => {
  Event.find({})
    .then((result) => {
      res.json({
        status: 'SUCCESS',
        data: result,
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while fetching all Events',
      });
    });
});

module.exports = router;
