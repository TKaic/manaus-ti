const express = require('express');
const router = express.Router();
const Local = require('./../models/Local');
const Event = require('./../models/Event');

router.post('/create', (req, res) => {
  const { endereco, numeroTelefone, horario, informacoes, imagens, nome, categorias } = req.body;

  const newLocal = new Local({
    nome,
    endereco,
    numeroTelefone,
    horario,
    informacoes,
    imagens,
    categorias,
  });

  newLocal
    .save()
    .then((result) => {
      res.json({
        status: 'SUCCESS',
        message: 'Local created successfully',
        data: result,
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while creating a local',
      });
    });
});

router.put('/edit/:id', (req, res) => {
  const { endereco, numeroTelefone, horario, informacoes } = req.body;

  Local.findByIdAndUpdate(
    req.params.id,
    {
      endereco,
      numeroTelefone,
      horario,
      informacoes,
    },
    { new: true }
  )
    .then((result) => {
      res.json({
        status: 'SUCCESS',
        message: 'Local updated successfully',
        data: result,
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while updating the local',
      });
    });
});

router.delete('/delete/:id', (req, res) => {
  Local.findByIdAndDelete(req.params.id)
    .then(() => {
      res.json({
        status: 'SUCCESS',
        message: 'Local deleted successfully',
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while deleting the local',
      });
    });
});

router.get('/get/:id', (req, res) => {
  Local.findById(req.params.id)
    .then((result) => {
      res.json({
        status: 'SUCCESS',
        data: result,
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while fetching the local',
      });
    });
});

router.get('/getall', (req, res) => {
  Local.find({})
    .then((result) => {
      res.json({
        status: 'SUCCESS',
        data: result,
      });
    })
    .catch((err) => {
      res.json({
        status: 'FAILED',
        message: 'An error occurred while fetching all locals',
      });
    });
});

// router.get('/buscar', (req, res) => {
//   const termo = req.query.termo;

//   // Utiliza uma expressão regular para realizar a busca flexível
//   const regex = new RegExp(termo, 'i');

//   // Realiza a busca no banco de dados
//   Local.find({ $or: [{ nome: regex }, { categorias: regex }] })
//     .then((result) => {
//       res.json({
//         status: 'SUCCESS',
//         data: result,
//       });
//     })
//     .catch((err) => {
//       res.json({
//         status: 'FAILED',
//         message: 'An error occurred while searching for locals',
//       });
//     });
// });

router.get('/buscar', async (req, res) => {
  try {
    const termo = req.query.termo;

    const regex = new RegExp(termo, 'i');

    const locais = await Local.find({ $or: [{ nome: regex }, { categorias: regex }] });
    const eventos = await Event.find({ $or: [{ nome: regex }, { categorias: regex }] });

    res.json({
      status: 'SUCCESS',
      data: { locais, eventos },
    });
  } catch (error) {
    console.error('Erro ao buscar locais e eventos:', error);
    res.status(500).json({ status: 'FAILED', message: 'Erro interno do servidor' });
  }
});


module.exports = router;
