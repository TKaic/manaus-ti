const express = require("express");
const router = express.Router();

const User = require("./../models/User");
const Local = require("./../models/Local");

const bcrypt = require("bcrypt");

router.get("/findById/:userId", (req, res) => {
  const { userId } = req.params;

  User.findById(userId)
    .then((user) => {
      if (user) {
        res.json({
          status: "SUCCESS",
          data: user,
        });
      } else {
        res.json({
          status: "FAILED",
          message: "User not found",
        });
      }
    })
    .catch((error) => {
      console.error("Error finding user by ID:", error);
      res.json({
        status: "FAILED",
        message: "An error occurred while finding the user",
      });
    });
});

router.post("/addFavorite/:userId/:localId", async (req, res) => {
  const { userId, localId } = req.params;

  try {
    const user = await User.findById(userId);
    if (!user) {
      return res.json({
        status: "FAILED",
        message: "User not found",
      });
    }

    const index = user.favoriteLocals.indexOf(localId);
    if (index === -1) {
      user.favoriteLocals.push(localId);
    } else {
      user.favoriteLocals.splice(index, 1);
    }

    await user.save();

    res.json({
      status: "SUCCESS",
      data: user.favoriteLocals,
    });
  } catch (error) {
    console.error("Error adding/removing favorite:", error);
    res.json({
      status: "FAILED",
      message: "An error occurred while adding/removing the local to favorites",
    });
  }
});

router.get("/checkFavorite/:userId/:localId", async (req, res) => {
  const { userId, localId } = req.params;

  try {
    const user = await User.findById(userId);

    if (!user) {
      return res.json({
        status: "FAILED",
        message: "User not found",
      });
    }

    const isFavorite = user.favoriteLocals.includes(localId);

    res.json({
      status: "SUCCESS",
      data: {
        isFavorite: isFavorite,
      },
    });
  } catch (error) {
    console.error("Error checking favorite:", error);
    res.json({
      status: "FAILED",
      message: "An error occurred while checking the favorite status",
    });
  }
});

router.get("/getFavorites/:userId", async (req, res) => {
  const { userId } = req.params;

  try {
    const user = await User.findById(userId);
    if (!user) {
      return res.json({
        status: "FAILED",
        message: "User not found",
      });
    }

    const favoriteLocals = await Local.find({ _id: { $in: user.favoriteLocals } });

    res.json({
      status: "SUCCESS",
      data: favoriteLocals,
    });
  } catch (error) {
    console.error("Error getting favorite locals:", error);
    res.json({
      status: "FAILED",
      message: "An error occurred while getting favorite locals",
    });
  }
});

router.post("/edit/:userId", async (req, res) => {
  const { userId } = req.params;
  const { name } = req.body;

  try {
    const user = await User.findById(userId);
    
    if (!user) {
      return res.json({
        status: "FAILED",
        message: "User not found",
      });
    }

    user.name = name;

    await user.save();

    res.json({
      status: "SUCCESS",
      message: "User updated successfully",
      data: user,
    });
  } catch (error) {
    console.error("Error updating user:", error);
    res.json({
      status: "FAILED",
      message: "An error occurred while updating the user",
    });
  }
});

router.delete("/delete/:userId", async (req, res) => {
  const { userId } = req.params;

  try {
    const result = await User.deleteOne({ _id: userId });

    if (result.deletedCount === 0) {
      return res.json({
        status: "FAILED",
        message: "User not found",
      });
    }

    res.json({
      status: "SUCCESS",
      message: "User deleted successfully",
    });
  } catch (error) {
    console.error("Error deleting user:", error);
    res.json({
      status: "FAILED",
      message: "An error occurred while deleting the user",
    });
  }
});

router.post("/signup", (req, res) => {
  let { name, email, password } = req.body;
  name = name.trim();
  email = email.trim();
  password = password.trim();

  if (name == "" || email == "" || password == "") {
    res.json({
      status: "FAILED",
      message: "Empty input fields!",
    });
  } else if (!/^[a-zA-Z ]*$/.test(name)) {
    res.json({
      status: "FAILED",
      message: "Invalid name entered",
    });
  } else if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)) {
    res.json({
      status: "FAILED",
      message: "Invalid email entered",
    });
  } else if (password.length < 8) {
    res.json({
      status: "FAILED",
      message: "Password is too short!",
    });
  } else {
    User.find({ email })
      .then((result) => {
        if (result.length) {
          res.json({
            status: "FAILED",
            message: "User with the provided email already exists",
          });
        } else {
          const saltRounds = 10;
          bcrypt
            .hash(password, saltRounds)
            .then((hashedPassword) => {
              const newUser = new User({
                name,
                email,
                password: hashedPassword,
              });

              newUser
                .save()
                .then((result) => {
                  res.json({
                    status: "SUCCESS",
                    message: "Signup successful",
                    data: result,
                  });
                })
                .catch((err) => {
                  res.json({
                    status: "FAILED",
                    message: "An error occurred while saving user account!",
                  });
                });
            })
            .catch((err) => {
              res.json({
                status: "FAILED",
                message: "An error occurred while hashing password!",
              });
            });
        }
      })
      .catch((err) => {
        console.log(err);
        res.json({
          status: "FAILED",
          message: "An error occurred while checking for existing user!",
        });
      });
  }
});

router.post("/signin", (req, res) => {
  let { email, password } = req.body;
  email = email.trim();
  password = password.trim();

  if (email == "" || password == "") {
    res.json({
      status: "FAILED",
      message: "Empty credentials supplied",
    });
  } else {
    User.find({ email })
      .then((data) => {
        if (data.length) {

          const hashedPassword = data[0].password;
          bcrypt
            .compare(password, hashedPassword)
            .then((result) => {
              if (result) {
                res.json({
                  status: "SUCCESS",
                  message: "Signin successful",
                  data: data,
                });
              } else {
                res.json({
                  status: "FAILED",
                  message: "Invalid password entered!",
                });
              }
            })
            .catch((err) => {
              res.json({
                status: "FAILED",
                message: "An error occurred while comparing passwords",
              });
            });
        } else {
          res.json({
            status: "FAILED",
            message: "Invalid credentials entered!",
          });
        }
      })
      .catch((err) => {
        res.json({
          status: "FAILED",
          message: "An error occurred while checking for existing user",
        });
      });
  }
});

module.exports = router;